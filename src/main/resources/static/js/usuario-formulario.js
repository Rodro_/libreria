$(function(){
	$('#submit').click(function(){
		registrarUsuario();	
	});
});

function registrarUsuario(){
    $.ajax({
    	type: 'POST',
        url: 'registro/usuario',
        contentType: "application/json",
		data: 
			JSON.stringify({
				nombre:$("#nombre").val(),
				apellido:$("#apellido").val(),
				direccion:$("#direccion").val(),				
				ciudad:$("#ciudades").val(),
				codigo_area:$("#codioArea").val(),
				telefono:$("#telefono").val(),
				correo_electronico:$("#email").val(),
				nombre_usuario:$("#nombreUsuario").val(),
				contra:$("#contra").val(),
				confirmarcontra:$("#contra").val(),
				tipo_usuario:$("#tipoUsuario").val()
        	 }),
       		success: function(data){
       			alert("Usuario registrado con EXITO!");
			},
			error: function(data){
				alert("Registro invalido");
			}
    });
}