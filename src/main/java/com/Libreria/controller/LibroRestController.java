package com.Libreria.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Libreria.entity.Autor;
import com.Libreria.entity.Libro;
import com.Libreria.repo.Autorrepo;
import com.Libreria.repo.LibroRepo;


@RestController
@RequestMapping("/buscar")
public class LibroRestController {
	
	@Autowired
	private LibroRepo repositorio;
	
	@Autowired
	private Autorrepo repositorioAutor;

	
	@GetMapping("/libro") 
	public List<Libro> getLibros(){
		return repositorio.findAll(); 
	}
	
	@GetMapping("/libro/titulo/{titulo}")
	public ResponseEntity<Libro> getLibroPorTitulo(@PathVariable String titulo){
		Optional<Libro> libro = repositorio.findByTitulo(titulo);  
		if(!libro.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(libro.get());
	}

	@GetMapping("/libro/autor/{nombreAutor}")
	public ResponseEntity<Autor> getLibroPorAutor(@PathVariable(value="nombreAutor") String nombre_autor){
		Optional<Autor> libro = repositorioAutor.findByNombre(nombre_autor);
		if(!libro.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(libro.get());
	}
	
	@GetMapping("/libro/isbn/{isbn}")
	public ResponseEntity<Libro> getLibroPorISBN(@PathVariable String isbn){
		Optional<Libro> libro = repositorio.findByIsbn(isbn);  
		if(!libro.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(libro.get());
	}
	
	
	/*
	@PostMapping("/usuario")
	public ResponseEntity<Usuario> insertarUsuario(@RequestBody Usuario usuario){
		Usuario usuarioRegistrado = repositorio.save(usuario);
		return ResponseEntity.ok().body(usuarioRegistrado);
	}*/
}
