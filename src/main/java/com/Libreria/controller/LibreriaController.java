package com.Libreria.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LibreriaController {
	@GetMapping("/libreria")
	public String index() {
		return "inicio";
	}
}
