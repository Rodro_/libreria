package com.Libreria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Libreria.entity.Usuario;
import com.Libreria.repo.UsuarioRepo;

@RestController
@RequestMapping("/registro")
public class UsuarioRestController {
	
	@Autowired
	private UsuarioRepo repositorio;
	
	@PostMapping("/usuario")
	public ResponseEntity<Usuario> insertarUsuario(@RequestBody Usuario usuario){
		Usuario usuarioRegistrado = repositorio.save(usuario);
		return ResponseEntity.ok().body(usuarioRegistrado);
	}

}	
