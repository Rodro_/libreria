package com.Libreria.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Libreria.entity.Autor;

public interface Autorrepo extends JpaRepository<Autor, Long>{
	Optional<Autor> findByNombre(String nombre);
}
