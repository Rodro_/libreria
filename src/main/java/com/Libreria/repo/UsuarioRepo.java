package com.Libreria.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Libreria.entity.Usuario;

public interface UsuarioRepo extends JpaRepository<Usuario, Long> {
}
