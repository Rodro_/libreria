package com.Libreria.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Libreria.entity.Categoria;

public interface CategoriaRepo extends JpaRepository<Categoria, Long>{

}
