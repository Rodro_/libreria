package com.Libreria.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.Libreria.entity.Libro;


public interface LibroRepo extends JpaRepository<Libro, Long>{
	Optional<Libro> findByTitulo(String titulo);
	Optional<Libro> findByIsbn( String isbn);
}
