package com.Libreria.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="publicador")
public class Publicador {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idpublicador;
	
	//Relación de uno a muchos con la tabla "DetallePedido" bien echa.
	@OneToMany(mappedBy="publicador")
	private Set<Libro> libro;
	
	@Column private String nombre;
	public Publicador() {
		super();
	}
	public Publicador(Long idpublicador, String nombre) {
		super();
		this.idpublicador = idpublicador;
		this.nombre = nombre;
	}
	public Long getIdpublicador() {
		return idpublicador;
	}
	public void setIdpublicador(Long idpublicador) {
		this.idpublicador = idpublicador;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}	
