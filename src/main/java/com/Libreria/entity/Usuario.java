package com.Libreria.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Length;


@Entity
@Table (name="usuario")
public class Usuario {
	@Id
	@Length(max=30)
	private String nombre_usuario;

	@Column(name="identificacion") 
	private long id;
	@Column (length = 15)
	private String nombre;
	@Column (length = 15)
	private String apellido;
	@Column(length = 45)
	private String direccion;
	@Column(length = 30) 
	private String correo_electronico;
	@Column(name="contrasena", length = 20) 
	private String contra;
	@Transient 
	private String confirmarcontra;
	@Column(length = 15)
	private String tipo_usuario;
	@Column(name="codigo_de_area") 
	private long codigo_area;
	@Column 
	private long telefono;
	@Column (length = 50)
	private String ciudad;
	@Column(name="departamento", length = 20) 
	private String estado;
	@Column(name="codigo_postal") 
	private long codigo_postal;
	@Column
	private boolean modo_un_click;


	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "numero_tarjeta", unique=true)
	private TarjetaCredito tarjetaCredito;
	

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")//Esta variable "usuario" es la que está en la entidad pedido.(se mapea)
    private List<Pedido> pedidos;	
	
	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Usuario(String nombre_usuario, long id, String nombre, String apellido, String direccion,
			String correo_electronico, String contra, String confirmarcontra, String tipo_usuario, long codigo_area,
			long telefono, String ciudad, String estado, long codigo_postal, boolean modo_un_click,
			TarjetaCredito tarjetaCredito) {
		super();
		this.nombre_usuario = nombre_usuario;
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.correo_electronico = correo_electronico;
		this.contra = contra;
		this.confirmarcontra = confirmarcontra;
		this.tipo_usuario = tipo_usuario;
		this.codigo_area = codigo_area;
		this.telefono = telefono;
		this.ciudad = ciudad;
		this.estado = estado;
		this.codigo_postal = codigo_postal;
		this.modo_un_click = modo_un_click;
		this.tarjetaCredito = tarjetaCredito;
	}

	public String getNombre_usuario() {
		return nombre_usuario;
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

	public String getContra() {
		return contra;
	}

	public void setContra(String contra) {
		this.contra = contra;
	}

	public String getConfirmarcontra() {
		return confirmarcontra;
	}

	public void setConfirmarcontra(String confirmarcontra) {
		this.confirmarcontra = confirmarcontra;
	}

	public String getTipo_usuario() {
		return tipo_usuario;
	}

	public void setTipo_usuario(String tipo_usuario) {
		this.tipo_usuario = tipo_usuario;
	}

	public long getCodigo_area() {
		return codigo_area;
	}

	public void setCodigo_area(long codigo_area) {
		this.codigo_area = codigo_area;
	}

	public long getTelefono() {
		return telefono;
	}

	public void setTelefono(long telefono) {
	this.telefono = telefono;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public long getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(long codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public boolean isModo_un_click() {
		return modo_un_click;
	}

	public void setModo_un_click(boolean modo_un_click) {
		this.modo_un_click = modo_un_click;
	}

	public TarjetaCredito getTarjetaCredito() {
		return tarjetaCredito;
	}

	public void setTarjetaCredito(TarjetaCredito tarjetaCredito) {
		this.tarjetaCredito = tarjetaCredito;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	
}
