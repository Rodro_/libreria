package com.Libreria.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="autor")
public class Autor {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idautor;
	
	@Column private String nombre;
	
	@JsonIgnoreProperties("autor")
	@OneToMany(mappedBy="autor")
	private Set<Libro> libro;
	
	public Autor() {
		super();
	}
	public Set<Libro> getLibro() {
		return libro;
	}
	public void setLibro(Set<Libro> libro) {
		this.libro = libro;
	}
	public Autor(long idautor, String nombre) {
		super();
		this.idautor = idautor;
		this.nombre = nombre;
	}
	public long getIdautor() {
		return idautor;
	}
	public void setIdautor(long idautor) {
		this.idautor = idautor;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
