package com.Libreria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name="tarjetaCredito")
public class TarjetaCredito {
	@Id
	private Long numero_tarjeta;
	
	@Column private String tipo_tarjeta;
	@Column private String expir;
	@Column private String cvc;
	
	@OneToOne(mappedBy = "tarjetaCredito")
    private Usuario usuario;
	
	public TarjetaCredito(long numero_tarjeta, String tipo_tarjeta, String expir, String cvc) {
		super();
		this.numero_tarjeta = numero_tarjeta;
		this.tipo_tarjeta = tipo_tarjeta;
		this.expir = expir;
		this.cvc = cvc;
	}
	
	public TarjetaCredito() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getNumero_tarjeta() {
		return numero_tarjeta;
	}
	public void setNumero_tarjeta(long numero_tarjeta) {
		this.numero_tarjeta = numero_tarjeta;
	}
	public String getTipo_tarjeta() {
		return tipo_tarjeta;
	}
	public void setTipo_tarjeta(String tipo_tarjeta) {
		this.tipo_tarjeta = tipo_tarjeta;
	}
	public String getExpir() {
		return expir;
	}
	public void setExpir(String expir) {
		this.expir = expir;
	}
	public String getCvc() {
		return cvc;
	}
	public void setCvc(String cvc) {
		this.cvc = cvc;
	}
}
