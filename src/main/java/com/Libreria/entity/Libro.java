package com.Libreria.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table (name="libro")
public class Libro {
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	private Long idlibro;
	
	@Column private String titulo;
	@Column private String descripcion;
	@Column private String fecha;
	@Column private int calificacion;
	@Column private String isbn;
	@Column private int cantidad;
	@Column private Long   precio;
	
	@ManyToMany
	@JoinTable(name="libro_categoria", 
				joinColumns=@JoinColumn(name="idlibro"),
				inverseJoinColumns=@JoinColumn(name="idcategoria")
	)
	private Set<Categoria> categorias;
	
	@JsonIgnoreProperties("libro")
	@ManyToOne
	@JoinColumn(name="idautor")
	private Autor autor;
	
	@ManyToOne
	@JoinColumn(name="idpublicador")
	private Publicador publicador;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "libro")
	private List<DetallePedido> detallepedidos; 
	
	public Libro() {
		super();
	}

	public Libro(Long idlibro, String titulo, String descripcion, String fecha, int calificacion, String isbn,
			Long precio, Set<Categoria> categorias, Autor autor, Publicador publicador, int cantidad) {
		super();
		this.idlibro = idlibro;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.calificacion = calificacion;
		this.isbn = isbn;
		this.precio = precio;
		this.categorias = categorias;
		this.autor = autor;
		this.publicador = publicador;
		this.cantidad = cantidad;
	}
	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	public Set<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(Set<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Long getIdlibro() {
		return idlibro;
	}

	public void setIdlibro(Long idlibro) {
		this.idlibro = idlibro;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String iSBN) {
		isbn = iSBN;
	}

	public Long getPrecio() {
		return precio;
	}

	public void setPrecio(Long precio) {
		this.precio = precio;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public Publicador getPublicador() {
		return publicador;
	}

	public void setPublicador(Publicador publicador) {
		this.publicador = publicador;
	}

}
