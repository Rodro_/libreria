package com.Libreria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="detallepedido")
public class DetallePedido {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long iddetalle;
	
	@Column private int cantidad;
	
	//Relación de Muchos a uno con la tabla "Pedido" bien echa.(entidad propietaria).
	 @ManyToOne
	 @JoinColumn(name="idpedido")
	 private Pedido pedido;
	 
	//Relación de Muchos a uno con la tabla "Libro" bien echa.(entidad propietaria).
	 @ManyToOne
	 @JoinColumn(name="idlibro")
	 private Libro libro;

	public DetallePedido() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DetallePedido(int cantidad, Pedido pedido, Libro libro) {
		super();
		this.cantidad = cantidad;
		this.pedido = pedido;
		this.libro = libro;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
}
