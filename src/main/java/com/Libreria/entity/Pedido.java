package com.Libreria.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="pedido")
public class Pedido {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idpedido;
	
	@Column private int codigo;
	@Column(length=15) private String metodo_envio;
	@Column private float costo;

	//Relación de uno a muchos con la tabla usuario bien echa (entidad propietaria).
    @ManyToOne
    @JoinColumn(name="nombre_usuario")
    private Usuario usuario;
    
  //Relación de uno a muchos con la tabla "DetallePedido" bien echa.
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pedido")
	private List<DetallePedido> detallepedidos; 

	public Pedido() {
		super();
	}

	public Pedido(long idpedido, int codigo, String metodo_envio, float costo) {
		super();
		this.idpedido = idpedido;
		this.codigo = codigo;
		this.metodo_envio = metodo_envio;
		this.costo = costo;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public long getIdpedido() {
		return idpedido;
	}
	public void setIdpedido(long idpedido) {
		this.idpedido = idpedido;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getMetodo_envio() {
		return metodo_envio;
	}
	public void setMetodo_envio(String metodo_envio) {
		this.metodo_envio = metodo_envio;
	}
	public float getCosto() {
		return costo;
	}
	public void setCosto(float costo) {
		this.costo = costo;
	}
	
	
	
}
